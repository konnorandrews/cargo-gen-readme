use std::cell::RefCell;
use std::collections::HashMap;

use comrak::nodes::{Ast, AstNode, LineColumn, NodeValue};
use comrak::{parse_document, Arena};
use indexmap::IndexMap;

use crate::models::DocSection;

pub fn parse_doc(text: &str) -> DocSection {
    let arena = Arena::new();

    let root = parse_document(&arena, text, &comrak::Options::default());

    // Clean up code blocks.
    for node in root.descendants() {
        match &mut node.data.borrow_mut().value {
            NodeValue::CodeBlock(block) => match &*block.info {
                "text" => block.info = "".into(),
                "" | "ignore" | "should_panic" | "no_run" | "compile_fail" | "edition2015"
                | "edition2018" | "edition2021" => {
                    block.info = "rust".into();
                    block.literal = block
                        .literal
                        .lines()
                        .filter(|line| !line.trim().starts_with("#"))
                        .collect::<Vec<_>>()
                        .join("\n");
                }
                _ => {}
            },
            _ => {}
        }
    }

    parse_doc_inner(root)
}

fn parse_doc_inner<'a>(node: &'a AstNode<'a>) -> DocSection {
    let mut stack = Vec::<(&AstNode, u8, Vec<(String, DocSection, &AstNode)>)>::new();
    stack.push((node, 0, Vec::new()));

    let mut children = node.children();

    loop {
        let current = stack.last().unwrap();
        if let Some(child) = children.next() {
            match &child.data.borrow().value {
                NodeValue::Heading(heading) => {
                    let arena = Arena::new();

                    // Keep finishing sections until we get to a section with a lower
                    // level.
                    let mut current_level = current.1;
                    while current_level >= heading.level {
                        let section = stack.pop().unwrap();
                        current_level = stack.last().unwrap().1;

                        let title = format_heading(section.0);

                        let prefix_end = if let Some((_, _, end)) = section.2.first() {
                            end
                        } else {
                            child
                        };
                        let prefix = sub_document(
                            section.0.next_sibling().unwrap(),
                            Some(prefix_end),
                            &arena,
                        );

                        let document =
                            sub_document(section.0.next_sibling().unwrap(), Some(child), &arena);

                        let mut buf = Vec::new();
                        comrak::format_commonmark(prefix, &comrak::Options::default(), &mut buf)
                            .unwrap();
                        let prefix = String::from_utf8(buf).unwrap();

                        let mut buf = Vec::new();
                        comrak::format_commonmark(document, &comrak::Options::default(), &mut buf)
                            .unwrap();
                        let text = String::from_utf8(buf).unwrap();

                        stack.last_mut().unwrap().2.push((
                            title,
                            DocSection {
                                prefix,
                                full: text,
                                children: convert_section_list(section.2),
                            },
                            section.0,
                        ));
                    }

                    // This heading starts a child section.
                    stack.push((child, heading.level, Vec::new()));
                }
                _ => {} // It wasn't a header so we don't care.
            }
        } else {
            let arena = Arena::new();

            // No more nodes, end all open sections.
            while stack.len() > 1 {
                // The child section needs to be added to the parent's list.

                let inner = stack.pop().unwrap();

                let title = format_heading(inner.0);

                if let Some(next) = inner.0.next_sibling() {
                    let document = sub_document(next, None, &arena);

                    let prefix_end = if let Some((_, _, end)) = inner.2.first() {
                        Some(*end)
                    } else {
                        None
                    };
                    let prefix = sub_document(inner.0.next_sibling().unwrap(), prefix_end, &arena);

                    let mut buf = Vec::new();
                    comrak::format_commonmark(prefix, &comrak::Options::default(), &mut buf)
                        .unwrap();
                    let prefix = String::from_utf8(buf).unwrap();

                    let mut buf = Vec::new();
                    comrak::format_commonmark(document, &comrak::Options::default(), &mut buf)
                        .unwrap();
                    let text = String::from_utf8(buf).unwrap();

                    stack.last_mut().unwrap().2.push((
                        title,
                        DocSection {
                            prefix,
                            full: text,
                            children: convert_section_list(inner.2),
                        },
                        inner.0,
                    ));
                }
            }

            // Last section is the root.

            let root = stack.pop().unwrap();

            let prefix_end = if let Some((_, _, end)) = root.2.first() {
                Some(*end)
            } else {
                None
            };
            let prefix = if let Some(start) = node.first_child() {
                let prefix = sub_document(start, prefix_end, &arena);

                let mut buf = Vec::new();
                comrak::format_commonmark(prefix, &comrak::Options::default(), &mut buf).unwrap();
                String::from_utf8(buf).unwrap()
            } else {
                String::new()
            };

            let mut buf = Vec::new();
            comrak::format_commonmark(root.0, &comrak::Options::default(), &mut buf).unwrap();
            let text = String::from_utf8(buf).unwrap();

            return DocSection {
                prefix,
                full: text,
                children: convert_section_list(root.2),
            };
        }
    }
}

fn convert_section_list(
    sections: Vec<(String, DocSection, &AstNode)>,
) -> IndexMap<String, DocSection> {
    let mut count = HashMap::new();

    sections
        .into_iter()
        .map(|(title, content, _)| {
            let n = count.entry(title.clone()).or_insert(0);
            let x = *n;
            *n += 1;

            if x == 0 {
                (title, content)
            } else {
                (format!("{}_{}", title, x), content)
            }
        })
        .collect()
}

fn format_heading<'a>(node: &'a AstNode<'a>) -> String {
    let mut title = String::new();

    for child in node.children() {
        match &child.data.borrow().value {
            NodeValue::Text(text) => title.push_str(text),
            NodeValue::TaskItem(check) => {
                if let Some(check) = check {
                    title.push('[');
                    title.push(*check);
                    title.push_str("] ");
                } else {
                    title.push_str("[ ] ");
                }
            }
            NodeValue::Code(code) => title.push_str(&code.literal),
            NodeValue::Link(link) => title.push_str(&link.title),
            NodeValue::Image(image) => title.push_str(&image.title),
            _ => {}
        }
    }

    title
}

fn sub_document<'a, 'b>(
    start_node: &'a AstNode<'a>,
    end_node: Option<&'a AstNode<'a>>,
    arena: &'b Arena<AstNode<'b>>,
) -> &'b AstNode<'b> {
    let root = arena.alloc(AstNode::new(RefCell::new(Ast::new(
        NodeValue::Document,
        LineColumn { line: 0, column: 0 },
    ))));

    // Clone all the nodes to the new document.
    if let Some(end_node) = end_node {
        let mut current = start_node;
        while !current.same_node(end_node) {
            root.append(clone_node(current, arena));

            if let Some(next) = current.next_sibling() {
                current = next;
            } else {
                break;
            };
        }
    } else {
        let mut current = start_node;
        root.append(clone_node(current, arena));

        while let Some(next) = current.next_sibling() {
            root.append(clone_node(next, arena));
            current = next;
        }
    }

    root
}

fn clone_node<'a, 'b>(node: &'a AstNode<'a>, arena: &'b Arena<AstNode<'b>>) -> &'b AstNode<'b> {
    // Clone the not graph data.
    let value = node.data.borrow().clone();

    // Create a node with the cloned data.
    let parent = arena.alloc(AstNode::new(RefCell::new(value)));

    // Clone all the children nodes.
    for child in node.children() {
        parent.append(clone_node(child, arena));
    }

    parent
}
