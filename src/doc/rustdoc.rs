use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::path::Path;
use std::process::Stdio;

use indexmap::IndexMap;
use rustdoc_types::{
    Crate, Enum, Id, Impl, ItemEnum, Module, Primitive, Struct, StructKind, Trait, Union,
};
use serde::{Deserialize, Serialize};

use super::markdown::parse_doc;
use crate::models::{Children, DocSection, ItemDoc};
use crate::runner::{CargoError, Nothing, Runner};

#[derive(thiserror::Error, Debug)]
pub enum RustdocError {
    #[error("IO error calling rustdoc: {0}")]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Cargo(#[from] CargoError),

    #[error("Error parsing json: {0}")]
    Json(#[from] serde_json::Error),
}

#[derive(Debug, Copy, Clone)]
pub enum TargetKind {
    Libary,
    Binary,
    Example,
    Test,
    Bench,
}

pub fn load_docs(
    target_name: &str,
    required_features: &[String],
    package_name: &str,
    kind: TargetKind,
    runner: &Runner,
    manifest_path: &Path,
    target_dir: &Path,
) -> Result<(Option<String>, ItemDoc), RustdocError> {
    match kind {
        TargetKind::Bench | TargetKind::Test => {
            return Ok((
                None,
                ItemDoc {
                    doc: DocSection {
                        prefix: String::new(),
                        full: String::new(),
                        children: IndexMap::new(),
                    },
                    children: Children::default(),
                },
            ))
        }
        _ => {}
    }

    // Put the artifacts in the target dir so they are cached.
    // But don't use the normal doc folder so we don't overwrite normal docs.
    // let output_dir = target_dir.join("gen_readme").join(package_name).join(target_name);
    let output_dir = target_dir.join("gen_readme").join(package_name);

    runner
        .try_with_cargo_nightly(|command| {
            // Run cargo's rustdoc command so we can tell it to use json.
            command.arg("rustdoc");

            // Set the target dir to use.
            command.arg("--target-dir").arg(&output_dir);

            // Force cargo to use this manifest.
            command.arg("--manifest-path").arg(&manifest_path);

            // Select the package the target is a part of.
            command.args(["--package", package_name]);

            for feature in required_features {
                command.arg("--features").arg(feature);
            }

            // TODO: remove this
            command.arg("--all-features");

            // Select the target.
            match kind {
                TargetKind::Libary => command.arg("--lib"),
                TargetKind::Binary => command.args(["--bin", target_name]),
                TargetKind::Example => command.args(["--example", target_name]),
                TargetKind::Test => command.args(["--test", target_name]),
                TargetKind::Bench => command.args(["--bench", target_name]),
            };

            // Set to output in json.
            command.args(["--", "-Z", "unstable-options", "--output-format", "json"]);

            Ok::<_, Nothing>(())
        })
        .map_err(|err| match err {
            crate::runner::WithCustom::Base(err) => err,
            crate::runner::WithCustom::Custom(_) => unreachable!(),
        })?;

    // Cargo tells rustdoc to use the doc folder.
    let doc_path = output_dir
        .join("doc")
        .join(&(target_name.replace("-", "_") + ".json"));

    let docs = serde_json::from_slice::<Crate>(&std::fs::read(doc_path)?)?;

    let (_, name, doc) = parse(&docs, &docs.root);
    Ok((Some(name), doc))
}

fn parse<'a>(root: &'a Crate, id: &Id) -> (&'a ItemEnum, String, ItemDoc) {
    // Pull item from the index.
    let item = &root.index[id];

    // Try to get a name for the item.
    let name = if let Some(name) = &item.name {
        name.clone()
    } else {
        match &item.inner {
            ItemEnum::ExternCrate { name, rename } => rename.as_ref().unwrap_or(name).clone(),
            // ItemEnum::Import(Import { name, .. }) => name.clone(),
            ItemEnum::Macro(name) => name.clone(),
            ItemEnum::Primitive(Primitive { name, .. }) => name.clone(),
            ItemEnum::Impl(Impl { trait_, .. }) => {
                if let Some(path) = trait_ {
                    path.name.clone()
                } else {
                    String::new()
                }
            }
            _ => String::new(),
        }
    };

    let mut children = Children::default();
    match &item.inner {
        ItemEnum::Module(Module { items, .. }) => {
            for id in items {
                children.insert(parse(root, id));
            }
        }
        ItemEnum::Union(Union { fields, impls, .. }) => {
            for id in fields {
                children.insert(parse(root, id));
            }

            for id in impls {
                children.insert(parse(root, id));
            }
        }
        ItemEnum::Struct(Struct { impls, kind, .. }) => {
            match kind {
                StructKind::Unit => {}
                StructKind::Tuple(fields) => {
                    for id in fields {
                        if let Some(id) = id {
                            children.insert(parse(root, id));
                        }
                    }
                }
                StructKind::Plain { fields, .. } => {
                    for id in fields {
                        children.insert(parse(root, id));
                    }
                }
            }

            for id in impls {
                children.insert(parse(root, id));
            }
        }
        ItemEnum::Enum(Enum {
            variants, impls, ..
        }) => {
            for id in variants {
                children.insert(parse(root, id));
            }

            for id in impls {
                children.insert(parse(root, id));
            }
        }
        ItemEnum::Trait(Trait {
            items,
            implementations,
            ..
        }) => {
            for id in items {
                children.insert(parse(root, id));
            }

            for id in implementations {
                children.insert(parse(root, id));
            }
        }
        ItemEnum::Impl(Impl { items, .. }) => {
            for id in items {
                children.insert(parse(root, id));
            }
        }
        ItemEnum::Primitive(Primitive { impls, .. }) => {
            for id in impls {
                children.insert(parse(root, id));
            }
        }
        _ => {} // All other items have no children.
    }

    (
        &item.inner,
        name,
        ItemDoc {
            doc: parse_doc(item.docs.as_deref().unwrap_or("")),
            children,
        },
    )
}

impl Children {
    fn insert_into(map: &mut HashMap<String, ItemDoc>, name: String, doc: ItemDoc) {
        let insert_name = if name.is_empty() {
            String::from("_0")
        } else {
            name.clone()
        };

        match map.entry(insert_name.clone()) {
            Entry::Occupied(_) => {
                // Find a name we can use by adding `_N`.
                let mut count = 1;
                let mut new_name;
                loop {
                    new_name = format!("{}_{}", name, count);
                    if map.contains_key(&new_name) {
                        count += 1
                    } else {
                        break;
                    }
                }
                map.insert(new_name, doc);
            }
            Entry::Vacant(entry) => {
                entry.insert(doc);
            }
        }
    }

    pub fn insert(&mut self, (kind, name, doc): (&ItemEnum, String, ItemDoc)) {
        let map = match kind {
            ItemEnum::Module(_) => &mut self.modules,
            ItemEnum::ExternCrate { .. } => &mut self.extern_crates,
            // ItemEnum::Import(_) => &mut self.imports,
            ItemEnum::Union(_) => &mut self.unions,
            ItemEnum::Struct(_) => &mut self.structs,
            ItemEnum::StructField(_) => &mut self.struct_fields,
            ItemEnum::Enum(_) => &mut self.enums,
            ItemEnum::Variant(_) => &mut self.variants,
            ItemEnum::Function(_) => &mut self.functions,
            ItemEnum::Trait(_) => &mut self.traits,
            ItemEnum::TraitAlias(_) => &mut self.trait_aliases,
            ItemEnum::Impl(_) => &mut self.impls,
            ItemEnum::TypeAlias(_) => &mut self.type_aliases,
            // ItemEnum::OpaqueTy(_) => &mut self.opaque_types,
            ItemEnum::Constant { .. } => &mut self.constants,
            ItemEnum::Static(_) => &mut self.statics,
            // ItemEnum::ForeignType => &mut self.foreign_types,
            ItemEnum::Macro(_) => &mut self.macros,
            ItemEnum::ProcMacro(_) => &mut self.proc_macros,
            ItemEnum::Primitive(_) => &mut self.primitives,
            ItemEnum::AssocConst { .. } => &mut self.assoc_consts,
            ItemEnum::AssocType { .. } => &mut self.assoc_types,
            ItemEnum::Use(_) => &mut self.imports,
            ItemEnum::ExternType => &mut self.foreign_types,
        };

        Self::insert_into(map, name, doc);
    }
}
