pub mod headers;
pub mod table;
pub mod var;
pub mod case;

use std::collections::HashMap;
use std::fmt::Display;

use handlebars::{handlebars_helper, no_escape, Handlebars};
use miette::{Diagnostic, LabeledSpan, SourceOffset};
use serde::Serialize;
use var::Var;

use self::table::Table;
use crate::template::headers::Headers;

/// Handlebars template renderer specialized for generating READMEs.
///
/// Supports miette diagnostics for it's errors.
///
/// # Extra Helpers
/// ...
pub struct Renderer<'reg> {
    /// The sources of the templates.
    template_sources: HashMap<String, String>,

    /// The handlebar registry.
    reg: Handlebars<'reg>,
}

impl<'reg> Renderer<'reg> {
    /// Create new renderer instance.
    pub fn new() -> Self {
        let mut reg = Handlebars::new();

        // We want errors if a template uses something that doesn't exist.
        reg.set_strict_mode(true);

        // Don't escape because we are making markdown.
        reg.register_escape_fn(no_escape);

        // Add custom helpers.
        // reg.register_helper("json", Box::new(Json));
        reg.register_helper("var", Box::new(Var::new()));
        reg.register_helper("table", Box::new(Table));
        reg.register_helper("headers", Box::new(Headers));
        reg.register_helper("case", Box::new(case::case));

        handlebars_helper!(json: |x: Json| serde_json::to_string(x).unwrap());
        reg.register_helper("json", Box::new(json));

        Self {
            template_sources: HashMap::new(),
            reg,
        }
    }

    /// Register a template string.
    ///
    /// This can be rendered directly or be used by another template.
    pub fn register_template_string(
        &mut self,
        name: &str,
        tpl_str: impl AsRef<str>,
    ) -> Result<(), TemplateError> {
        // Record the source text.
        self.template_sources
            .insert(name.to_owned(), tpl_str.as_ref().to_owned());

        // Add to registry.
        self.reg
            .register_template_string(name, tpl_str)
            .map_err(|err| TemplateError::new(HandlebarsError::Template(err), self))
    }

    /// Render a registered template with the given data.
    pub fn render<T>(&self, name: &str, data: &T) -> Result<String, TemplateError>
    where
        T: Serialize,
    {
        self.reg
            .render(name, data)
            .map_err(|err| TemplateError::new(HandlebarsError::Render(err), self))
    }

    /// Get the source text of a registered template.
    pub fn template_source(&self, name: &str) -> Option<&str> {
        self.template_sources.get(name).map(String::as_str)
    }
}

/// Error with Handlebars template.
#[derive(thiserror::Error, Debug)]
pub struct TemplateError {
    /// Source text of the template the error happened in.
    template_source: Option<String>,

    /// The error.
    error: HandlebarsError,
}

impl Display for TemplateError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // The diagnostic output gives the reason so we don't put it here.
        match &self.error {
            HandlebarsError::Render(_) => write!(f, "Error rendering Handlebars template."),
            HandlebarsError::Template(_) => write!(f, "Error parsing Handlebars template."),
        }
    }
}

/// Kinds of Handlebars errors.
#[derive(Debug)]
pub enum HandlebarsError {
    /// An error rendering a template.
    Render(handlebars::RenderError),

    /// An error parsing a template.
    Template(handlebars::TemplateError),
}

impl HandlebarsError {
    /// The name of the template the error happened in.
    pub fn template_name(&self) -> Option<&str> {
        match self {
            HandlebarsError::Render(err) => err.template_name.as_deref(),
            HandlebarsError::Template(err) => err.template_name.as_deref(),
        }
    }

    /// The line number the error happened at.
    pub fn line_no(&self) -> Option<usize> {
        match self {
            HandlebarsError::Render(err) => err.line_no,
            HandlebarsError::Template(err) => err.line_no,
        }
    }

    /// The column number the error happened at.
    pub fn column_no(&self) -> Option<usize> {
        match self {
            HandlebarsError::Render(err) => err.column_no,
            HandlebarsError::Template(err) => err.column_no,
        }
    }

    /// The reason the error happened.
    pub fn reason(&self) -> String {
        match self {
            HandlebarsError::Render(err) => err.desc.clone(),
            HandlebarsError::Template(err) => err.reason().to_string(),
        }
    }
}

impl TemplateError {
    fn new(error: HandlebarsError, renderer: &Renderer) -> Self {
        Self {
            template_source: error
                .template_name()
                .and_then(|name| renderer.template_source(name))
                .map(str::to_owned),
            error,
        }
    }
}

impl Diagnostic for TemplateError {
    fn code<'a>(&'a self) -> Option<Box<dyn std::fmt::Display + 'a>> {
        match &self.error {
            HandlebarsError::Render(_) => Some(Box::new("template::render")),
            HandlebarsError::Template(_) => Some(Box::new("template::parse")),
        }
    }

    fn severity(&self) -> Option<miette::Severity> {
        Some(miette::Severity::Error)
    }

    fn help<'a>(&'a self) -> Option<Box<dyn std::fmt::Display + 'a>> {
        // If anything is missing for a label we emit a help with the reason.
        if self.template_source.is_none()
            || self.error.line_no().is_none()
            || self.error.column_no().is_none()
        {
            Some(Box::new(format!("Reason: {}", self.error.reason())))
        } else {
            None
        }
    }

    fn url<'a>(&'a self) -> Option<Box<dyn std::fmt::Display + 'a>> {
        None
    }

    fn source_code(&self) -> Option<&dyn miette::SourceCode> {
        self.template_source.as_ref().map(|x| x as _)
    }

    fn labels(&self) -> Option<Box<dyn Iterator<Item = miette::LabeledSpan> + '_>> {
        // Emit the code that handlebars said caused the error.
        if let (Some(source), Some(line_no), Some(column_no)) = (
            &self.template_source,
            self.error.line_no(),
            self.error.column_no(),
        ) {
            let offset = SourceOffset::from_location(source, line_no, column_no);
            if offset.offset() == source.len() {
                // Emit the entire source as handlebars said it happened after the
                // source.
                let span = LabeledSpan::new(Some(self.error.reason()), 0, source.len());
                Some(Box::new(std::iter::once(span)))
            } else {
                let span = LabeledSpan::new(Some(self.error.reason()), offset.offset(), 0);
                Some(Box::new(std::iter::once(span)))
            }
        } else {
            None
        }
    }

    fn related<'a>(&'a self) -> Option<Box<dyn Iterator<Item = &'a dyn Diagnostic> + 'a>> {
        None
    }

    fn diagnostic_source(&self) -> Option<&dyn Diagnostic> {
        None
    }
}
