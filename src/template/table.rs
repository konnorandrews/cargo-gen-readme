use handlebars::{
    Context, Handlebars, Helper, HelperDef, Output, RenderContext, RenderError, Renderable,
    StringOutput,
};
use markdown_table_formatter::format_tables;

pub struct Table;

impl HelperDef for Table {
    fn call<'reg: 'rc, 'rc>(
        &self,
        h: &Helper<'reg, 'rc>,
        r: &'reg Handlebars<'reg>,
        c: &'rc Context,
        rc: &mut RenderContext<'reg, 'rc>,
        output: &mut dyn Output,
    ) -> Result<(), RenderError> {
        if let Some(template) = h.template() {
            let mut temp = StringOutput::new();
            template.render(r, c, rc, &mut temp)?;
            let text = temp.into_string()?;
            output.write(&format_tables(text))?;
        }

        Ok(())
    }
}
