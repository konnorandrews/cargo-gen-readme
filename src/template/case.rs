use handlebars::handlebars_helper;
use heck::{ToKebabCase as _, ToLowerCamelCase as _, ToShoutyKebabCase as _, ToShoutySnakeCase as _, ToSnekCase as _, ToTitleCase as _, ToTrainCase as _, ToUpperCamelCase as _};

handlebars_helper!(case: |text: str, kind: str| match kind {
    "UpperCamelCase" => text.to_upper_camel_case(),
    "lowerCamelCase" => text.to_lower_camel_case(),
    "snake_case" => text.to_snek_case(),
    "kebab-case" => text.to_kebab_case(),
    "SHOUTY_SNAKE_CASE" => text.to_shouty_snake_case(),
    "Title Case" => text.to_title_case(),
    "SHOUTY-KEBAB-CASE" => text.to_shouty_kebab_case(),
    "Train-Case" => text.to_train_case(),
    _ => text.to_string()
});
