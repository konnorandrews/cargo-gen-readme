use std::collections::HashMap;
use std::sync::Mutex;

use handlebars::{Context, Handlebars, Helper, HelperDef, RenderContext, RenderError, ScopedJson};
use serde_json::Value;

/// Handlebar helper to set and get variables.
///
/// Variables are seperate from the context and are global.
/// Set a variable with `{{var "name" value}}`, and get a variable's value
/// with `{{var "name"}}`.
pub struct Var {
    /// Variable values.
    vars: Mutex<HashMap<String, Value>>,
}

impl Var {
    pub fn new() -> Self {
        Self {
            vars: Mutex::new(HashMap::new()),
        }
    }
}

impl HelperDef for Var {
    fn call_inner<'reg: 'rc, 'rc>(
        &self,
        h: &Helper<'reg, 'rc>,
        r: &'reg Handlebars<'reg>,
        _: &'rc Context,
        _: &mut RenderContext<'reg, 'rc>,
    ) -> Result<handlebars::ScopedJson<'reg, 'rc>, RenderError> {
        // First param is the variable name.
        let name = h
            .param(0)
            .and_then(|x| {
                if r.strict_mode() && x.is_value_missing() {
                    None
                } else {
                    Some(x.value())
                }
            })
            .ok_or_else(|| {
                RenderError::new(&format!("`Var` helper: Couldn't read parameter name",))
            })?;

        // Get it as a string.
        let name = name
            .as_str()
            .ok_or_else(|| {
                RenderError::new(&format!("`Var` helper: Parameter name is not a string",))
            })?
            .to_owned();

        // Check if a value was given.
        if let Some(value) = h.param(1) {
            // We need the value to exist in strict mode.
            if r.strict_mode() && value.is_value_missing() {
                return Err(RenderError::new(
                    "`Var` helper: Couldn't read parameter value",
                ));
            }

            // Store the value.
            self.vars
                .lock()
                .unwrap()
                .insert(name, value.value().clone());

            // Storing a value returns no value to render.
            Ok(ScopedJson::Derived(Value::Null))
        } else {
            // Return the variable's value.
            Ok(self
                .vars
                .lock()
                .unwrap()
                .get(&name)
                .map(|json| ScopedJson::Derived(json.clone()))
                .unwrap_or(ScopedJson::Missing))
        }
    }
}
