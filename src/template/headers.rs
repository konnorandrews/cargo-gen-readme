use comrak::nodes::NodeValue;
use comrak::{parse_document, Arena};
use handlebars::{
    Context, Handlebars, Helper, HelperDef, Output, RenderContext, RenderError, Renderable,
    StringOutput,
};
use markdown_table_formatter::format_tables;

pub struct Headers;

impl HelperDef for Headers {
    fn call<'reg: 'rc, 'rc>(
        &self,
        h: &Helper<'reg, 'rc>,
        r: &'reg Handlebars<'reg>,
        c: &'rc Context,
        rc: &mut RenderContext<'reg, 'rc>,
        output: &mut dyn Output,
    ) -> Result<(), RenderError> {
        // First param is the heading delta.
        let delta = h
            .param(0)
            .and_then(|x| {
                if r.strict_mode() && x.is_value_missing() {
                    None
                } else {
                    Some(x.value())
                }
            })
            .ok_or_else(|| {
                RenderError::new(&format!("`Var` helper: Couldn't read parameter delta",))
            })?;

        // Get it as a int.
        let delta = delta
            .as_i64()
            .ok_or_else(|| {
                RenderError::new(&format!("`Var` helper: Parameter name is not an integer",))
            })?
            .to_owned() as u8;

        if let Some(template) = h.template() {
            let mut temp = StringOutput::new();
            template.render(r, c, rc, &mut temp)?;
            let text = temp.into_string()?;

            let arena = Arena::new();
            let root = parse_document(&arena, &text, &comrak::Options::default());
            for node in root.descendants() {
                match &mut node.data.borrow_mut().value {
                    NodeValue::Heading(heading) => {
                        if heading.setext && heading.level.saturating_add(delta) > 2 {
                            heading.setext = false;
                        }

                        heading.level = heading.level.saturating_add(delta);
                    }
                    _ => {}
                }
            }
            let mut buf = Vec::new();
            comrak::format_commonmark(root, &comrak::Options::default(), &mut buf)?;
            let buf = String::from_utf8(buf)?;

            output.write(&buf)?;
        }

        Ok(())
    }
}
