//! Automatically find a way to run cargo.

use std::ffi::{OsStr, OsString};
use std::path::PathBuf;
use std::process::{Command, Stdio};

use directories::{BaseDirs, UserDirs};

/// Runner for Cargo.
///
/// This manages figuring out how to run a Cargo instance.
#[derive(Debug)]
pub struct Runner {
    /// Info from the discovery phase.
    info: RunnerInfo,
}

/// Info for how to invoke cargo.
#[derive(Debug)]
enum RunnerInfo {
    /// Invoke cargo via rustup.
    ///
    /// This is preferred so we can set nightly.
    Rustup(OsString),

    /// Invoke cargo directly.
    Cargo(OsString),
}

#[derive(thiserror::Error, Debug)]
pub enum Nothing {}

#[derive(thiserror::Error, Debug)]
pub enum WithCustom<B, T = Nothing> {
    #[error(transparent)]
    Base(#[from] B),

    #[error(transparent)]
    Custom(T),
}

#[derive(thiserror::Error, Debug)]
#[error("error running command `{command}`")]
#[non_exhaustive]
pub struct CargoError {
    pub command: String,

    #[source]
    pub kind: CargoErrorKind,
}

fn format_command(command: &Command) -> String {
    let mut line = String::new();

    line.push_str(&command.get_program().to_string_lossy());

    for arg in command.get_args() {
        line.push(' ');
        line.push_str(&arg.to_string_lossy());
    }

    line
}

#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum CargoErrorKind {
    #[error("{0}")]
    Stderr(String),

    #[error("Error parsing buffer as utf-8.")]
    Utf8(#[from] std::string::FromUtf8Error),

    #[error(transparent)]
    Io(#[from] std::io::Error),
}

#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
#[error("Unable to find runnable `rustup` or `cargo`.")]
pub struct RunnerDetectError {}

impl Runner {
    /// Find a runner.
    ///
    /// This will try to find a runnable `rustup` or `cargo`.
    /// As a result, this function can cause side effects as it tries running
    /// potential `rustup` and `cargo` binaries.
    pub fn detect() -> Result<Self, RunnerDetectError> {
        // First try to find a runnable rustup.
        if let Some(this) = try_find_rustup() {
            return Ok(this);
        }

        // First try to find a runnable cargo.
        if let Some(this) = try_find_cargo() {
            return Ok(this);
        }

        // Neither were found.
        Err(RunnerDetectError {})
    }

    /// Create command to call a nightly `cargo`.
    ///
    /// If only a `cargo` binary was found by [`Self::detect`] we assume it's a nightly cargo.
    pub fn cargo_nightly(&self) -> Command {
        match &self.info {
            RunnerInfo::Rustup(bin) => {
                // Invoke a nightly cargo.
                let mut command = Command::new(bin);
                command.args(["run", "nightly", "cargo"]);
                command
            }
            RunnerInfo::Cargo(bin) => Command::new(bin), // Assume it's nightly.
        }
    }

    pub fn try_with_cargo_nightly<F, E>(&self, func: F) -> Result<String, WithCustom<CargoError, E>>
    where
        F: FnOnce(&mut Command) -> Result<(), E>,
    {
        let mut command = self.cargo_nightly();

        func(&mut command).map_err(WithCustom::Custom)?;

        let line = format_command(&command);

        let output = command
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .output()
            .map_err(CargoErrorKind::Io)
            .map_err(|kind| CargoError {
                command: line.clone(),
                kind,
            })?;

        if output.status.success() {
            // Convert to string.
            Ok(String::from_utf8(output.stdout)
                .map_err(CargoErrorKind::Utf8)
                .map_err(|kind| CargoError {
                    command: line,
                    kind,
                })?)
        } else {
            // Convert to string.
            let msg = String::from_utf8(output.stderr)
                .map_err(CargoErrorKind::Utf8)
                .map_err(|kind| CargoError {
                    command: line.clone(),
                    kind,
                })?;

            // Remove the extra error prefix cargo adds.
            let msg = msg
                .strip_prefix("error:")
                .unwrap_or(msg.as_str())
                .trim()
                .to_owned();

            Err(CargoError {
                command: line,
                kind: CargoErrorKind::Stderr(msg),
            }
            .into())
        }
    }

    fn new_rustup(bin: OsString) -> Self {
        Self {
            info: RunnerInfo::Rustup(bin),
        }
    }

    fn new_cargo(bin: OsString) -> Self {
        Self {
            info: RunnerInfo::Cargo(bin),
        }
    }
}

fn try_find_cargo() -> Option<Runner> {
    // Cargo's help has the word `package`.
    let check_bin = |bin: &OsStr| check_bin(bin, "package");

    // Check the environment variable if it's given.
    let cargo = std::env::var_os("CARGO");
    if let Some(cargo) = cargo {
        if check_bin(&cargo) {
            return Some(Runner::new_cargo(cargo));
        }
    }

    // Check a basic call to cargo.
    let cargo = OsString::from("cargo");
    if check_bin(&cargo) {
        return Some(Runner::new_cargo(cargo));
    }

    // Check for the CARGO_HOME to find cargo.
    let cargo_home = std::env::var_os("CARGO_HOME");
    if let Some(cargo_home) = cargo_home {
        let cargo = PathBuf::from(cargo_home)
            .join("bin")
            .join("cargo")
            .into_os_string();

        if check_bin(&cargo) {
            return Some(Runner::new_cargo(cargo));
        }
    }

    // Check manually in the user's home folder for cargo.
    if let Some(user_dirs) = UserDirs::new() {
        let cargo = user_dirs
            .home_dir()
            .join(".cargo")
            .join("bin")
            .join("cargo")
            .into_os_string();

        if check_bin(&cargo) {
            return Some(Runner::new_cargo(cargo));
        }
    }

    // Check for a system wide cargo.
    if let Some(base_dirs) = BaseDirs::new() {
        if let Some(executable) = base_dirs.executable_dir() {
            let cargo = executable.join("cargo").into_os_string();

            if check_bin(&cargo) {
                return Some(Runner::new_cargo(cargo));
            }
        }
    }

    // We didn't find a callable cargo.
    None
}

fn try_find_rustup() -> Option<Runner> {
    // Rustup's help contains the word `toolchain`.
    let check_bin = |bin: &OsStr| check_bin(bin, "toolchain");

    // Check the environment variable if it's given.
    let rustup = std::env::var_os("RUSTUP");
    if let Some(rustup) = rustup {
        if check_bin(&rustup) {
            return Some(Runner::new_rustup(rustup));
        }
    }

    // Check a basic call to rustup.
    let rustup = OsString::from("rustup");
    if check_bin(&rustup) {
        return Some(Runner::new_rustup(rustup));
    }

    // Check for the CARGO_HOME to find rustup.
    let cargo_home = std::env::var_os("CARGO_HOME");
    if let Some(cargo_home) = cargo_home {
        let rustup = PathBuf::from(cargo_home)
            .join("bin")
            .join("rustup")
            .into_os_string();

        if check_bin(&rustup) {
            return Some(Runner::new_rustup(rustup));
        }
    }

    // Check manually in the user's home folder for rustup.
    if let Some(user_dirs) = UserDirs::new() {
        let rustup = user_dirs
            .home_dir()
            .join(".cargo")
            .join("bin")
            .join("rustup")
            .into_os_string();

        if check_bin(&rustup) {
            return Some(Runner::new_rustup(rustup));
        }
    }

    // Check for a system wide rustup.
    if let Some(base_dirs) = BaseDirs::new() {
        if let Some(executable) = base_dirs.executable_dir() {
            let rustup = executable.join("rustup").into_os_string();

            if check_bin(&rustup) {
                return Some(Runner::new_rustup(rustup));
            }
        }
    }

    // We didn't find a callable rustup.
    None
}

/// Check that a binary can be ran.
///
/// This passes `--help` as the only argument.
///
/// `needle` is a string to search for in the stdout.
/// This checks that the program we ran is probably what we wanted.
fn check_bin(path: &OsStr, needle: &str) -> bool {
    if let Ok(output) = Command::new(path)
        .arg("--help")
        .stdout(Stdio::piped())
        .output()
    {
        if let Ok(output) = String::from_utf8(output.stdout) {
            if output.contains(needle) {
                return true;
            }
        }
    }

    false
}
