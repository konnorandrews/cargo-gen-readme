//! This is a generator for readme files.
//!
//! ...
//!
//! end
//!
//! # Examples
//!
//! ```
//! println!("a");
//! println!("b");
//!
//! println!("c");
//! ```

use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Display;
use std::path::PathBuf;
use std::sync::mpsc::{channel, RecvTimeoutError};
use std::sync::Mutex;
use std::time::Duration;

use argh::FromArgs;
use cargo_gen_readme::template::Renderer;
use cargo_gen_readme::{load_workspace_info, Event};
use comrak::parse_document;
use handlebars::{
    handlebars_helper, no_escape, Context, Handlebars, Helper, HelperDef, HelperResult, Output,
    RenderContext, RenderError, ScopedJson,
};
use indicatif::{ProgressBar, ProgressStyle};
use miette::{Diagnostic, IntoDiagnostic, LabeledSpan, NamedSource, Result, SourceOffset};
use serde::Serialize;
use serde_json::Value;

/// Generate README from docs.
#[derive(FromArgs)]
struct Args {
    /// output file
    #[argh(option, short = 'o')]
    output: Option<PathBuf>,

    /// directory to search for a Cargo package
    #[argh(option, short = 'r')]
    project_root: Option<PathBuf>,

    /// template file to use
    #[argh(option, short = 't')]
    template: Option<PathBuf>,
}

fn main() -> Result<()> {
    let args: Args = argh::from_env();

    let project_root = if let Some(root) = args.project_root {
        root
    } else {
        std::env::current_dir().into_diagnostic()?
    };

    let bar = ProgressBar::new(0);
    bar.set_style(
        ProgressStyle::with_template("{pos:>3}/{len:3} {spinner} {bar:20.cyan/blue} {msg}")
            .unwrap()
            .progress_chars("##-"),
    );

    let info = std::thread::scope(|scope| {
        let (send, recv) = channel();

        scope.spawn(move || loop {
            match recv.recv_timeout(Duration::from_millis(60)) {
                Ok(event) => match event {
                    Event::RunningRustdoc {
                        package_name,
                        target_name,
                        total,
                    } => {
                        bar.set_length(total);
                        bar.inc(1);
                        bar.set_message(format!("{} - {}", package_name, target_name));
                    }
                    _ => {}
                },
                Err(RecvTimeoutError::Timeout) => bar.tick(),
                Err(RecvTimeoutError::Disconnected) => break,
            }
        });

        load_workspace_info(project_root, move |event| {
            send.send(event).unwrap();
        })
        .into_diagnostic()
    })?;

    handlebars_helper!(json: |value: Value| serde_json::to_string_pretty(&value).unwrap());

    let mut renderer = Renderer::new();

    renderer.register_template_string("readme", include_str!("../templates/readme.hbs"))?;

    let text = renderer.render("readme", &info)?;
    println!("{}", text);

    // let arena = comrak::Arena::new();
    // let mut options = comrak::Options::default();
    // options.render.escape = false;
    // options.extension.table = true;
    // let mut formatted_text = Vec::new();
    // comrak::format_commonmark(parse_document(&arena, &text, &options), &options, &mut formatted_text).unwrap();
    // println!("{}", String::from_utf8(formatted_text).unwrap());

    Ok(())
}
