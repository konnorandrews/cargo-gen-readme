use std::collections::HashMap;
use std::str::FromStr;

use cargo_metadata::camino::Utf8PathBuf;
use cargo_metadata::semver::{Version, VersionReq};
use cargo_metadata::{DependencyKind, Edition};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};

/// Info about a dependency.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Dependency {
    /// The original name if it was renamed.
    pub original_name: Option<String>,

    /// The kind of dependency.
    pub kind: DependencyKind,

    /// If the dependency is optional.
    pub optional: bool,

    /// The required version.
    pub req: VersionReq,

    /// The target triple for the dependency.
    pub target: Option<String>,

    /// The registry url.
    pub registry: Option<String>,

    /// The features enabled.
    pub features: Vec<String>,

    /// If the default features are used.
    pub uses_default_features: bool,
}

/// Info about a parsed license.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub enum ParsedLicense {
    /// A known SPDX license.
    Known {
        /// Short name of the license.
        name: String,

        /// Human friendly name.
        full_name: String,

        /// Name of the exception.
        exception: Option<String>,

        /// The full text of the license.
        text: Option<String>,

        /// The full text of the exception.
        exception_text: Option<String>,
    },

    /// Not a known SPDX license.
    Unknown {
        /// The name given.
        name: String,

        /// The reference document.
        doc: Option<String>,
    },
}

/// Info about a license.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub enum License {
    /// The license expression was parsed.
    Parsed {
        /// The raw expression.
        expression: String,

        /// The individial licenses and their full texts.
        requirements: IndexMap<String, ParsedLicense>,
    },

    /// The license is unknown.
    Unknown {
        /// The raw expression.
        expression: Option<String>,

        /// The license file path relative to the workspace root.
        file: Option<Utf8PathBuf>,
    },
}

/// Info about a target.
///
/// A target is a library, binary, example, ...
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Target {
    /// The kind of target.
    ///
    /// Libraries can have multiple kinds at the same time.
    pub kind: Vec<String>,

    /// The required features for the target.
    pub required_features: Vec<String>,

    /// Path to the main source file of the target relative to the workspace root.
    pub src_path: Utf8PathBuf,

    /// Target edition.
    pub edition: Edition,

    /// If docs are automatically built for the target.
    pub doc: bool,

    /// Name of the target in it's docs.
    pub doc_name: Option<String>,

    /// All the markdown docs for the target and it's items.
    pub docs: ItemDoc,
}

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Library {
    pub name: String,

    #[serde(flatten)]
    pub target: Target,
}

/// Info about a author.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Author {
    /// Name of the author.
    pub name: String,

    /// The author's email.
    pub email: Option<String>,
}

impl FromStr for Author {
    type Err = std::io::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim();

        if s.chars().last() == Some('>') {
            if let Some((name, email)) = s.rsplit_once("<") {
                Ok(Self {
                    name: name.trim().to_owned(),
                    email: Some(email[..email.len() - 1].trim().to_owned()),
                })
            } else {
                Ok(Self {
                    name: s.to_owned(),
                    email: None,
                })
            }
        } else {
            Ok(Self {
                name: s.to_owned(),
                email: None,
            })
        }
    }
}

/// Info about a cargo package.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Package {
    /// Version of the package.
    pub version: Version,

    /// Authors of the package.
    pub authors: Vec<Author>,

    /// Description of the package.
    pub description: Option<String>,

    /// Dependencies of the package.
    ///
    /// The name is the renamed or the original if not renamed.
    pub dependencies: HashMap<String, Dependency>,

    /// License the package has.
    pub license: License,

    /// The lib target in the package.
    pub lib: Option<Library>,

    pub bins: HashMap<String, Target>,

    /// The example targets in the package.
    pub examples: HashMap<String, Target>,

    /// The test targets in the package.
    pub tests: HashMap<String, Target>,

    /// The bench targets in the package.
    pub benches: HashMap<String, Target>,

    /// The features.
    pub features: HashMap<String, Vec<String>>,

    /// Path to the package toml relative to the workspace root.
    pub manifest_path: Utf8PathBuf,

    /// Category keywords for the package.
    pub categories: Vec<String>,

    /// Keywords for the package.
    pub keywords: Vec<String>,

    /// Path to the readme for the package relative to the workspace root.
    pub readme: Option<Utf8PathBuf>,

    /// Repository link.
    pub repository: Option<String>,

    /// Homepage link.
    pub homepage: Option<String>,

    /// Documentation link.
    pub documentation: Option<String>,

    /// Edition the package is for.
    pub edition: Edition,

    /// Native dependencies that are linked.
    pub links: Option<String>,

    /// The default target to run.
    pub default_run: Option<String>,

    /// The version of Rust needed for the package.
    pub rust_version: Option<Version>,
}

/// Info about a cargo workspace.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct Workspace {
    /// The packages in the workspace.
    pub packages: HashMap<String, Package>,

    /// The default packages.
    pub workspace_default_members: Vec<String>,

    /// The workspace toml absolute path.
    pub workspace_root: Utf8PathBuf,

    /// The target directory cargo will use relative to the workspace root.
    pub target_directory: Utf8PathBuf,
}

/// Doc tree for an item.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct ItemDoc {
    /// The docs for the item split into sections.
    pub doc: DocSection,

    // Children items.
    #[serde(flatten)]
    pub children: Children,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[non_exhaustive]
pub struct Children {
    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub modules: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub extern_crates: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub imports: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub unions: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub structs: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub struct_fields: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub enums: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub variants: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub functions: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub traits: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub trait_aliases: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub impls: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub type_aliases: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub opaque_types: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub constants: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub statics: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub foreign_types: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub macros: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub proc_macros: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub primitives: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub assoc_consts: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub assoc_types: HashMap<String, ItemDoc>,

    #[serde(skip_serializing_if = "HashMap::is_empty")]
    pub implementations: HashMap<String, ItemDoc>,
}

/// Section of docs.
///
/// Each markdown heading is it's own section.
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
pub struct DocSection {
    /// The markdown before the first subheading.
    pub prefix: String,

    /// All the markdown including all subsections.
    pub full: String,

    /// The children sections.
    ///
    /// Each of these is formed by a subheading.
    pub children: IndexMap<String, DocSection>,
}
