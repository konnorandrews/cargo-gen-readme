//! Before
//!
//! # Name
//! After
//!
//! ## Sub heading
//!
//! ### Inner
//! Some content
//!
//! # Back to main
//! Some main content

mod doc;
mod models;
mod runner;
pub mod template;

// mod doc_parse;

use std::collections::HashMap;
use std::ffi::{OsStr, OsString};
use std::fs;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::sync::mpsc::Sender;
use std::sync::OnceLock;

use b::A;
use cargo_metadata::camino::{Utf8Path, Utf8PathBuf};
use cargo_metadata::semver::{Version, VersionReq};
use cargo_metadata::{DependencyKind, Edition};
use cargo_toml::Manifest;
use directories::{BaseDirs, UserDirs};
use doc::rustdoc::{load_docs, RustdocError, TargetKind};
// use doc_parse::Section;
use models::{DocSection, Library, License, ParsedLicense, Target, Workspace};
use runner::{CargoError, Nothing, RunnerDetectError};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use spdx::{Expression, LicenseItem, ParseMode};
use tempfile::tempdir;

use crate::{
    // doc_parse::Doc,
    models::{Dependency, Package},
    runner::Runner,
};

/// This is b.
pub mod b {
    /// This is A.
    ///
    /// ```ignore
    /// a
    /// ```
    ///
    /// ```text
    /// b
    /// ```
    pub struct A;

    /// Docs for the impl
    impl A {
        /// docs for the function
        pub fn new() -> Self {
            todo!()
        }
    }

    impl A {
        pub fn new2() -> Self {
            todo!()
        }
    }
}

#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum WorkspaceError {
    #[error("cannot access path `{0}`")]
    InvalidPath(PathBuf, #[source] std::io::Error),

    #[error("failed to get project metadata from Cargo")]
    CargoMetadata(#[from] CargoError),

    #[error("failed to generate docs")]
    Doc(#[from] RustdocError),

    // ---
    #[error("{0}")]
    Detect(#[from] RunnerDetectError),

    #[error("{0}")]
    Io(#[from] std::io::Error),

    #[error("{0}")]
    Json(#[from] serde_json::Error),

    #[error("{0}")]
    Toml(#[from] toml::de::Error),
}

#[non_exhaustive]
pub enum Event {
    RunningRustdoc {
        package_name: String,
        target_name: String,
        total: u64,
    },
}

pub fn load_workspace_info<P: AsRef<Path>, F: Fn(Event) + Clone>(
    path: P,
    on_event: F,
) -> Result<Workspace, WorkspaceError> {
    let path = path.as_ref();

    // Find a way to run cargo.
    let runner = Runner::detect()?;

    // Have cargo find the workspace manifest.
    let manifest_path = find_manifest_path(path, &runner)?;

    // Get the metadata for the workspace.
    let stdout = runner
        .try_with_cargo_nightly(|command| {
            command
                .args(["metadata", "--format-version", "1", "--manifest-path"])
                .arg(&manifest_path);
            Ok::<_, Nothing>(())
        })
        .map_err(|err| match err {
            runner::WithCustom::Base(err) => err,
            runner::WithCustom::Custom(_) => unreachable!(),
        })?;

    let manifest = serde_json::from_str::<cargo_metadata::Metadata>(&stdout)?;

    let relative_path = |path: &Utf8Path| {
        path.strip_prefix(&manifest.workspace_root)
            .unwrap_or(path)
            .to_path_buf()
    };

    let total_targets = manifest
        .workspace_members
        .iter()
        .map(|package_id| {
            let package = manifest
                .packages
                .iter()
                .find(|package| &package.id == package_id)
                .unwrap();

            package.targets.len()
        })
        .sum::<usize>() as _;

    Ok(Workspace {
        packages: manifest
            .workspace_members
            .iter()
            .map(|package_id| -> Result<(String, Package), WorkspaceError> {
                let package = manifest
                    .packages
                    .iter()
                    .find(|package| &package.id == package_id)
                    .unwrap();
                Ok((
                    package.name.clone(),
                    Package {
                        version: package.version.clone(),
                        authors: package
                            .authors
                            .iter()
                            .map(|author| author.parse().unwrap())
                            .collect(),
                        description: package.description.clone(),
                        dependencies: package
                            .dependencies
                            .iter()
                            .map(|dep| {
                                let name = dep.rename.as_ref().unwrap_or(&dep.name);
                                let original_name = dep.rename.is_some().then_some(&dep.name);

                                (
                                    name.clone(),
                                    Dependency {
                                        original_name: original_name.cloned(),
                                        kind: dep.kind,
                                        optional: dep.optional,
                                        req: dep.req.clone(),
                                        target: dep
                                            .target
                                            .as_ref()
                                            .map(|target| target.to_string()),
                                        registry: dep.registry.clone(),
                                        features: dep.features.clone(),
                                        uses_default_features: dep.uses_default_features,
                                    },
                                )
                            })
                            .collect(),
                        license: if let Some(license) = &package.license {
                            if let Ok(expr) = spdx::Expression::parse_mode(license, ParseMode::LAX)
                            {
                                License::Parsed {
                                    expression: expr.to_string(),
                                    requirements: expr
                                        .requirements()
                                        .map(|req| match &req.req.license {
                                            LicenseItem::Spdx { id, .. } => {
                                                let text = spdx::text::LICENSE_TEXTS
                                                    .iter()
                                                    .find(|(text_id, _)| *text_id == id.name)
                                                    .map(|(_, text)| text);

                                                let exception_text =
                                                    if let Some(exception_id) = req.req.exception {
                                                        spdx::text::EXCEPTION_TEXTS
                                                            .iter()
                                                            .find(|(ex_id, _)| {
                                                                *ex_id == exception_id.name
                                                            })
                                                            .map(|(_, text)| text)
                                                    } else {
                                                        None
                                                    };

                                                (
                                                    req.req.to_string(),
                                                    ParsedLicense::Known {
                                                        name: id.name.to_owned(),
                                                        full_name: id.full_name.to_owned(),
                                                        exception: req
                                                            .req
                                                            .exception
                                                            .map(|e| e.name.to_owned()),
                                                        text: text.map(|&s| s.to_owned()),
                                                        exception_text: exception_text
                                                            .map(|&s| s.to_owned()),
                                                    },
                                                )
                                            }
                                            LicenseItem::Other { doc_ref, lic_ref } => (
                                                req.req.to_string(),
                                                ParsedLicense::Unknown {
                                                    name: lic_ref.clone(),
                                                    doc: doc_ref.clone(),
                                                },
                                            ),
                                        })
                                        .collect(),
                                }
                            } else {
                                License::Unknown {
                                    expression: Some(license.clone()),
                                    file: package.license_file.clone(),
                                }
                            }
                        } else {
                            License::Unknown {
                                expression: None,
                                file: package.license_file.clone(),
                            }
                        },
                        lib: load_targets(
                            package,
                            TargetKind::Libary,
                            relative_path,
                            &manifest_path,
                            manifest.target_directory.as_std_path(),
                            &runner,
                            total_targets,
                            on_event.clone(),
                        )
                        .next()
                        .transpose()?
                        .map(|(name, target)| Library { name, target }),
                        bins: load_targets(
                            package,
                            TargetKind::Binary,
                            relative_path,
                            &manifest_path,
                            manifest.target_directory.as_std_path(),
                            &runner,
                            total_targets,
                            on_event.clone(),
                        )
                        .collect::<Result<_, _>>()?,
                        examples: load_targets(
                            package,
                            TargetKind::Example,
                            relative_path,
                            &manifest_path,
                            manifest.target_directory.as_std_path(),
                            &runner,
                            total_targets,
                            on_event.clone(),
                        )
                        .collect::<Result<_, _>>()?,
                        tests: load_targets(
                            package,
                            TargetKind::Test,
                            relative_path,
                            &manifest_path,
                            manifest.target_directory.as_std_path(),
                            &runner,
                            total_targets,
                            on_event.clone(),
                        )
                        .collect::<Result<_, _>>()?,
                        benches: load_targets(
                            package,
                            TargetKind::Bench,
                            relative_path,
                            &manifest_path,
                            manifest.target_directory.as_std_path(),
                            &runner,
                            total_targets,
                            on_event.clone(),
                        )
                        .collect::<Result<_, _>>()?,
                        features: package
                            .features
                            .iter()
                            .map(|(a, b)| (a.clone(), b.clone()))
                            .collect(),
                        manifest_path: relative_path(&package.manifest_path),
                        categories: package.categories.clone(),
                        keywords: package.keywords.clone(),
                        readme: package.readme.clone(),
                        repository: package.repository.clone(),
                        homepage: package.homepage.clone(),
                        documentation: package.documentation.clone(),
                        edition: package.edition.clone(),
                        links: package.links.clone(),
                        default_run: package.default_run.clone(),
                        rust_version: package.rust_version.clone(),
                    },
                ))
            })
            .collect::<Result<_, _>>()?,
        workspace_default_members: manifest
            .workspace_default_members
            .iter()
            .map(|id| {
                manifest
                    .packages
                    .iter()
                    .find(|package| &package.id == id)
                    .unwrap()
                    .name
                    .clone()
            })
            .collect(),
        workspace_root: manifest.workspace_root.clone(),
        target_directory: relative_path(&manifest.target_directory),
    })

    // let package = manifest.workspace_default_packages()[0];
    // let target = &package.targets[0];
    // dbg!(target);
    //
    // // let output_dir = tempdir()?;
    // let output_dir = manifest.target_directory.join("readdoc");
    //
    // let output = runner
    //     .cargo_nightly()
    //     .args(["rustdoc", "--lib", "--target-dir"])
    //     .arg(&output_dir)
    //     .args(["--", "-Z", "unstable-options", "--output-format", "json"])
    //     .stdout(Stdio::piped())
    //     .stderr(Stdio::piped())
    //     .output()?;
    // let docs = if output.status.success() {
    //     let doc_path = output_dir
    //         .join("doc")
    //         .join(&(target.name.replace("-", "_") + ".json"));
    //     // .join("cargo_readdoc.json");
    //     dbg!(&doc_path);
    //     serde_json::from_slice::<Doc>(&fs::read(doc_path)?)?
    // } else {
    //     return Err(parse_cargo_err(output.stderr)?);
    // };
    //
    // let root = &docs.index[&docs.root];
    // dbg!(root);
    // // let link = &docs.paths[&root.links["`A`"]];
    // // dbg!(link);
    //
    // // dbg!(
    // //     link.path[..link.path.len() - 1].join("/")
    // //         + "/"
    // //         + &link.kind
    // //         + "."
    // //         + link.path.last().unwrap()
    // //         + ".html"
    // // );
    //
    // print_section("crate", &docs.parse().section, "");
}

fn load_targets<'a, F: Fn(Event) + 'a>(
    package: &'a cargo_metadata::Package,
    kind: TargetKind,
    relative_path: impl Fn(&Utf8Path) -> Utf8PathBuf + 'a,
    manifest_path: &'a Path,
    target_dir: &'a Path,
    runner: &'a Runner,
    total: u64,
    on_event: F,
) -> impl Iterator<Item = Result<(String, Target), WorkspaceError>> + 'a {
    let kind_text = match kind {
        TargetKind::Libary => "lib",
        TargetKind::Binary => "bin",
        TargetKind::Example => "example",
        TargetKind::Test => "test",
        TargetKind::Bench => "bench",
    };

    package
        .targets
        .iter()
        .filter(move |target| target.kind.iter().any(|k| &**k == kind_text))
        .map(move |target| {
            on_event(Event::RunningRustdoc {
                package_name: package.name.clone(),
                target_name: target.name.clone(),
                total,
            });

            let (doc_name, docs) = load_docs(
                &target.name,
                &target.required_features,
                &package.name,
                kind,
                runner,
                manifest_path,
                target_dir,
            )?;

            Ok((
                target.name.clone(),
                Target {
                    kind: target.kind.clone(),
                    required_features: target.required_features.clone(),
                    src_path: relative_path(&target.src_path),
                    edition: target.edition,
                    doc: target.doc,
                    doc_name,
                    docs,
                },
            ))
        })
}

fn print_section(name: &str, section: &DocSection, indent: &str) {
    println!("{indent}Name: {name}");

    if !section.prefix.trim().is_empty() {
        println!("{indent}Content:");
        for line in section.prefix.lines() {
            println!("{indent}  {line}");
        }
    }

    if !section.children.is_empty() {
        println!("{indent}Children:");
        for (name, child) in &section.children {
            print_section(name, child, &format!("{indent}  "))
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields)]
struct Config {
    /// Sets if the workspace manifest is used.
    ///
    /// * `None` - Use the workspace manifest if it exists.
    /// * `Some(true)` - Force the workspace manifest to be used.
    /// * `Some(false)` - Use the project manifest. This allows multi-repo projects with workspaces.
    use_workspace: Option<bool>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            use_workspace: None,
        }
    }
}

#[derive(Deserialize, Debug)]
struct Metadata {
    #[serde(default)]
    readdoc: Config,
}

fn find_manifest_path(path: &Path, runner: &Runner) -> Result<PathBuf, WorkspaceError> {
    // Check that the path exists and resolve it into an absolute path we can get the parents
    // of.
    let mut path = path
        .canonicalize()
        .map_err(|err| WorkspaceError::InvalidPath(path.to_path_buf(), err))?;

    #[derive(Deserialize)]
    struct Locate {
        root: PathBuf,
    }

    if path.metadata()?.is_file() {
        // We were given a file so we need to back up a folder before searching.
        path.pop();
    }

    // Find the project Cargo.toml.
    let stdout = runner
        .try_with_cargo_nightly(|command| {
            command.arg("locate-project").current_dir(&path);
            Ok::<_, Nothing>(())
        })
        .map_err(|err| match err {
            runner::WithCustom::Base(err) => err,
            runner::WithCustom::Custom(_) => unreachable!(),
        })?;

    // Parse cargp's output json.
    let project = serde_json::from_str::<Locate>(&stdout)?.root;

    // Check if the workspace manifest should be use.
    let project_manifest = toml::from_str::<Manifest<Metadata>>(&fs::read_to_string(&project)?)?;
    let use_workspace = if let Some(package) = project_manifest.package {
        if let Some(metadata) = package.metadata {
            metadata.readdoc.use_workspace
        } else {
            None
        }
    } else {
        None
    };

    if matches!(use_workspace, None | Some(true)) {
        // Find the workspace Cargo.toml.
        let stdout = runner
            .try_with_cargo_nightly(|command| {
                command
                    .args(["locate-project", "--workspace"])
                    .current_dir(path);
                Ok::<_, Nothing>(())
            })
            .map_err(|err| match err {
                runner::WithCustom::Base(err) => err,
                runner::WithCustom::Custom(_) => unreachable!(),
            })?;

        // Parse cargp's output json.
        let workspace = serde_json::from_str::<Locate>(&stdout)?.root;

        // If use_workspace is Some(true) we force the workspace manifest we find to be
        // different.
        if use_workspace.is_some() && workspace == project {
            todo!()
            // Err(anyhow!(
            //     "Didn't find a cargo workspace manifest \
            //     seperate from package manifest `{}`. \
            //     Forced by `package.metadata.readdoc.use_workspace = true`.",
            //     project.display()
            // ))
        } else {
            Ok(workspace)
        }
    } else {
        Ok(project)
    }
}
