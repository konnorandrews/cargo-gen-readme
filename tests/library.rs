use std::path::PathBuf;

use cargo_gen_readme::{load_workspace_info, template::Renderer};

#[test]
fn demo() {
    let root = PathBuf::from(env!("CARGO_MANIFEST_DIR")).canonicalize().unwrap();

    let info = load_workspace_info(
        root.join("tests/example_library"),
        |_| (),
    )
    .unwrap();

    let root = root.to_string_lossy().into_owned();

    insta::with_settings!({
        filters => vec![
            (&*root, "<root>"),
        ],
        sort_maps => true,
    }, {
        insta::assert_ron_snapshot!(info, {
            ".packages.example_library.license.requirements.*.text" => "<license text>"
        });
    });

    let mut renderer = Renderer::new();

    renderer.register_template_string("readme", include_str!("../templates/readme.hbs")).unwrap();

    let text = renderer.render("readme", &info).unwrap();
    println!("{}", text);

    todo!()
}
