//! This is an example library.

/// An example function.
pub fn add(left: usize, right: usize) -> usize {
    left + right
}

/// An example struct.
pub struct X;
