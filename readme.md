# example_library

Example lib for testing.

![Version](https://img.shields.io/static/v1?label=version&message=1.2.3-alpha+meta-tag&color=informational)
[![docs.rs](https://img.shields.io/docsrs/example_library)](https://docs.rs/example_library/latest/example_library/)
[![Crates.io License](https://img.shields.io/crates/l/example_library)](#license)
[![Crates.io](https://img.shields.io/crates/v/example_library)](https://crates.io/crates/example_library)

This is an example library.

## Contributing

Contributions in any form (issues, pull requests, etc.) to this project must adhere to Rust's [Code of Conduct](https://www.rust-lang.org/policies/code-of-conduct).

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in `example_library` by you shall be licensed as below, without any additional terms or conditions.

## License

This project is licensed under either of

  * [Apache License 2.0](LICENSE-Apache-2.0)
  * [MIT License](LICENSE-MIT)

at your option.
